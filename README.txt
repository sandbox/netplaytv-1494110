Js CSS Remover
------------------------
Requires Drupal 6

Author: Marcelo Vani / Henri Grech-Cini
Copyright: NetplayTv

Overview:
--------
Remove un-necessary scripts/css allowing you to customize the includes per theme.

Features:
---------
Provides text area for Js and CSS where you can input a file on each line, the file can contain the full path
Settings are done individually for each theme

Installation:
-------------
Install the module
   
Configuration:
--------------
Go to the theme configuration i.e. /admin/build/themes/settings/garland
Input Scripts and Stylesheets to me removed. i.e. banner.js
Flush the caches

Testing
-------
Open the site and check the assets

Last updated:
------------
; $Id: README.txt,v 1.0 2012/03/15 10:00:37 mvani Exp $
