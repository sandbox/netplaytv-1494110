<?php
/**
 * @file
 * Adminstrative pages for JS CSS Remover Module
 * 
 * @copyright Copyright (C) 2012 NetPlayTV
 * @author Marcelo Vani <marcelo.vani@netplaytv.com>
 * @author Henri Grech-Cini<henry-grechcini@netplaytv.com>
 * @date    30/01/2012
 *
 */

 /**
 * Configuration form
 * 
 * @return
 * The configuration form 
 */ 
function jscss_remover_configuration_form() {
  $form['jscss_remover_configuration'] = array(
    '#type' => 'fieldset',
    '#title' => 'General configuration',
    '#collapsible' => TRUE,
  );
  
  $form['jscss_remover_configuration']['jscss_remover_selection_style'] = array(
    '#type' => 'radios',
    '#title' => t('Selection Style'),
    '#default_value' => variable_get('jscss_remover_selection_style', 0),
    '#options' => array(t('Text area'), t('Checkboxes (Under development)')),
    '#description' => 'Choose the way you want to select the Js/CSS on the <a href="/admin/themes/settings/' . variable_get('theme_default', '') . '#jscss-remover">Theme Configurations page</a>.',
  );  
   
  return system_settings_form($form);
}

